if empty(glob('~/.vim/autoload/plug.vim'))
  silent !curl -fLo ~/.vim/autoload/plug.vim --create-dirs
    \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
  autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif
call plug#begin('~/.vim/bundle')
Plug 'junegunn/vim-plug'
Plug 'majutsushi/tagbar'
Plug 'wannesm/wmgraphviz.vim'
Plug 'aklt/plantuml-syntax'
Plug 'octol/vim-cpp-enhanced-highlight'
Plug 'Yggdroot/indentLine'
Plug 'iamcco/mathjax-support-for-mkdp'
Plug 'iamcco/markdown-preview.vim'
Plug 'mileszs/ack.vim'
Plug 'SirVer/ultisnips'
Plug 'honza/vim-snippets'
Plug 'aperezdc/vim-template'
Plug 'thaerkh/vim-workspace'
Plug 'vim-scripts/repmo.vim'
Plug 'Shougo/neco-syntax'
Plug 'Shougo/neco-vim'
Plug 'Shougo/neoinclude.vim'
Plug 'vim-airline/vim-airline'
Plug 'scrooloose/nerdtree'
"Plug 'Shougo/neosnippet'
"Plug 'Shougo/neosnippet-snippets'
call plug#end()
