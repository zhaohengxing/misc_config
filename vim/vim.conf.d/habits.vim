set guifont=DejaVu\ Sans\ Mono\ 14
set encoding=utf-8
set fileencodings=ucs-bom,utf-8,gb2312,gbk,gb18030
"解决菜单乱码
source $VIMRUNTIME/delmenu.vim
source $VIMRUNTIME/menu.vim
"解决consle输出乱码
language messages zh_CN.utf-8
set tabstop=2
set shiftwidth=2
set nobackup
set noundofile
set ignorecase
set hidden
set bufhidden=hide
"set showtabline=2
set expandtab
set wic
set scrolloff=0
colorscheme desert
"au FileType c,cpp setl nu cinoptions+=t0 completeopt-=preview
"au FileType * set buflisted
"au BufAdd,BufNewFile * nested tab sball
"Emacs 风格的命令行
"cnoremap <C-A>		<Home>
"cnoremap <C-E>		<End>
"cnoremap <C-B>		<Left>
"cnoremap <C-F>		<Right>
"cnoremap <C-D>		<Del>
"cnoremap <Esc><C-B>	<S-Left>
"cnoremap <Esc><C-F>	<S-Right>
"map st :set filetype
"map cs :cs f
"nmap cd :cd %:p:h<cr>
